package de.dlr.gitlab.fame.logging;

import org.slf4j.Logger;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

public final class Logging {
	public static final Marker LOGGER_FATAL = MarkerFactory.getMarker("FATAL");
	static final String NO_INSTANCE = "Do not instantiate class: ";

	private Logging() {
		throw new IllegalStateException(NO_INSTANCE + getClass().getCanonicalName());
	}

	/** Returns a RuntimeException with given error message and logs a fatal error at given logger<br>
	 * <b>Important</b>: Remember to actually throw the returned Exception!
	 *
	 * @param logger to log the given message with
	 * @param errorMessage to be logged and added to exception
	 * @return {@link RuntimeException} with given error message */
	public static RuntimeException logFatalException(Logger logger, String errorMessage) {
		logger.error(LOGGER_FATAL, errorMessage);
		return new RuntimeException(errorMessage);
	}

	/** Throws the given uncaught Exception and logs its message at logging level FATAL
	 * 
	 * @param <T> Type of the exception
	 * @param logger logger to append the exception's message to
	 * @param exception the new Exception (including any specified exception message to be logged) */
	public static <T extends RuntimeException> void logAndThrowFatal(Logger logger, T exception) {
		logger.error(LOGGER_FATAL, exception.getMessage());
		throw exception;
	}

	/** Throws a new RuntimeException and logs its message at logging level FATAL
	 * 
	 * @param logger logger to append the exception's message to
	 * @param message the message to be logged and put on the thrown RuntimeException */
	public static void logAndThrowFatal(Logger logger, String message) {
		logAndThrowFatal(logger, new RuntimeException(message));
	}
}
