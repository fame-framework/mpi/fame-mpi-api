package de.dlr.gitlab.fame.mpi;

/** Simple interface for MPI Requests
 *
 * @author Christoph Schimeczek, Achraf El Ghazi */
public interface MpiRequestFacade {

	/** returns when MPI data transfer associated with this request has finished */
	abstract void waitForCompletion();
}
