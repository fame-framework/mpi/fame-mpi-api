package de.dlr.gitlab.fame.mpi;

import java.util.EnumMap;
import java.util.Set;
import org.reflections.Reflections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import de.dlr.gitlab.fame.logging.Logging;
import de.dlr.gitlab.fame.mpi.MpiFacade.MpiMode;

/** Find available implementations of MpiFacade and instantiates it
 * 
 * @author Christoph Schimeczek, Achraf El Ghazi */
public final class MpiInstantiator {
	private static final Logger LOGGER = LoggerFactory.getLogger(MpiInstantiator.class);
	private static final String DEFAULT_PACKAGE_NAME = "de.dlr.gitlab.fame.mpi";

	static final String MULTIPLE_IMPLEMENTATIONS = "Check dependencies! More than one MPI implementation found for same Mpi-mode: ";
	static final String IMPLEMENTATION_MISSING = "No MPI implementation found for mode: ";
	static final String INSTANTIATION_FAIL = "Could not instantiate MpiFacade, ensure default constructor exists and is accessible for: ";
	static final String MULTIPLE_PACKAGE_NAMES = "Does only support [0..1] package names!";

	/** Get an MPI implementation matching the given {@link MpiMode}
	 * 
	 * @param requestedMode parallel or sequential simulation execution
	 * @param packageNames optional parameter: used for <b>testing only</b>! Leave out in production
	 * @return implementation of MpiFacade matching requested mode */
	public static MpiFacade getMpi(MpiMode requestedMode, String... packageNames) {
		String packageName = extractPackageNames(packageNames);

		EnumMap<MpiMode, MpiFacade> facades = new EnumMap<>(MpiMode.class);
		for (Class<? extends MpiFacade> clas : getMpiImplementations(packageName)) {
			MpiFacade facade = instantiate(clas);
			mapsFacadeToMode(facade, facades);
		}
		if (!facades.containsKey(requestedMode)) {
			throw Logging.logFatalException(LOGGER, IMPLEMENTATION_MISSING + requestedMode);
		}
		return facades.get(requestedMode);
	}

	/** @returns if given name list is empty: {@link #DEFAULT_PACKAGE_NAME}; otherwise first packageName */
	private static String extractPackageNames(String[] packageNames) {
		switch (packageNames.length) {
			case 0:
				return DEFAULT_PACKAGE_NAME;
			case 1:
				return packageNames[0];
			default:
				throw Logging.logFatalException(LOGGER, MULTIPLE_PACKAGE_NAMES);
		}
	}

	/** @return Set of implementations of MpiFacade found on the class path */
	private static Set<Class<? extends MpiFacade>> getMpiImplementations(String packageName) {
		Reflections reflections = new Reflections(packageName);
		return reflections.getSubTypesOf(MpiFacade.class);
	}

	/** @return instantiated given MpiFacade class */
	private static MpiFacade instantiate(Class<? extends MpiFacade> clas) {
		try {
			return clas.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			throw Logging.logFatalException(LOGGER, INSTANTIATION_FAIL + clas);
		}
	}

	/** updates given map of facades to their MpiMode: adds given facade if corresponding MpiMode is not yet bound */
	private static void mapsFacadeToMode(MpiFacade facade, EnumMap<MpiMode, MpiFacade> facades) {
		MpiMode mpiMode = facade.getMode();
		if (!facades.containsKey(mpiMode)) {
			facades.put(mpiMode, facade);
		} else {
			throw Logging.logFatalException(LOGGER, MULTIPLE_IMPLEMENTATIONS + mpiMode);
		}
	}
}
