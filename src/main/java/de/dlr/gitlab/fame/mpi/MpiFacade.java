package de.dlr.gitlab.fame.mpi;

/** Serves as facade to the MessagePassingInterface (MPI)<br>
 *
 * @author Christoph Schimeczek, Achraf El Ghazi */
public interface MpiFacade {
	public enum MpiMode {
		/** Use MPI to run simulations one or more processes */
		PARALLEL,
		/** Use no MPI at all and run simulations on one process only */
		SINGLE_CORE,
		/** For testing purposes only */
		NOT_IMPLEMENTED
	};

	/** @return MPI mode associated with the MpiFacade implementation */
	abstract MpiMode getMode();

	/** Initialises MPI after its instantiation
	 * 
	 * @param args Parameter for MPI and main program
	 * @return Parameters for main program */
	public abstract String[] initialise(String[] args);

	/** @return MPI rank of this process */
	public abstract int getRank();

	/** @return Number of MPI processes */
	public abstract int getSize();

	/** Copies an array of Bytes from source process to all other processes
	 * 
	 * @param data byte array to be copied
	 * @param source processID hosting the original byte array
	 * @return On all processes: The copied byte array */
	public abstract byte[] broadcastBytes(byte[] data, int source);

	/** Sends an array of Bytes to the specified target process with given tag; <br>
	 * This method <b>blocks</b>, does not return until the receiver has accepted the message.
	 * 
	 * @param data byte array to be send
	 * @param target processID to receive the data
	 * @param tag data stream identifier */
	public abstract void sendBytesTo(byte[] data, int target, int tag);

	/** Receive an array of Bytes with a given tag
	 * 
	 * @param tag data stream identifier
	 * @return received byte array */
	public abstract byte[] receiveBytesWithTag(int tag);

	/** Sends an array of Bytes to the specified target process with given tag; <br>
	 * This method <b>does not block</b>, i.e. returns immediately after dispatching the data, not confirming its receival.
	 * 
	 * @param data byte array to be send
	 * @param target processID to receive the data
	 * @param tag data stream identifier
	 * @return Request object that allows waiting for the completion of the transmission */
	public abstract MpiRequestFacade iSendBytesTo(byte[] data, int target, int tag);

	/** Finalises MPI */
	public abstract void invokeFinalize();

}
