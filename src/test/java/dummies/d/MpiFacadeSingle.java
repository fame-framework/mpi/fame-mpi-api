package dummies.d;

import de.dlr.gitlab.fame.mpi.MpiFacade;
import de.dlr.gitlab.fame.mpi.MpiRequestFacade;

public class MpiFacadeSingle implements MpiFacade {

	@Override
	public byte[] broadcastBytes(byte[] data, int source) {
		return null;
	}

	@Override
	public void sendBytesTo(byte[] data, int target, int tag) {}

	@Override
	public byte[] receiveBytesWithTag(int tag) {
		return null;
	}

	@Override
	public MpiRequestFacade iSendBytesTo(byte[] data, int target, int tag) {
		return null;
	}

	@Override
	public int getRank() {
		return 0;
	}

	@Override
	public int getSize() {
		return 0;
	}

	@Override
	public String[] initialise(String[] args) {
		return null;
	}

	@Override
	public MpiMode getMode() {
		return MpiMode.SINGLE_CORE;
	}

	@Override
	public void invokeFinalize() {}
}
