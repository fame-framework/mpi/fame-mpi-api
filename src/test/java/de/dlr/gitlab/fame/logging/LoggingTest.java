package de.dlr.gitlab.fame.logging;


import static de.dlr.gitlab.fame.testUtils.ExceptionTesting.assertThrowsFatalMessage;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;

public class LoggingTest {
	private Logger logger;

	@Before
	public void setUp() {
		logger = mock(Logger.class);
	}

	@Test
	public void test_logFatalException_LoggerGetsError() {
		String errorMessage = "MyMessage";
		Logging.logFatalException(logger, errorMessage);
		verify(logger).error(Logging.LOGGER_FATAL, errorMessage);
	}

	@Test
	public void test_logFatalException_ReturnsRuntimeException() {
		String errorMessage = "MyMessage";
		RuntimeException re = Logging.logFatalException(logger, errorMessage);
		assertEquals(errorMessage, re.getMessage());
	}
	
	@Test
	public void test_logAndThrowFatal_LoggerGetsError() {
		String errorMessage = "MyMessage";
		try {
			Logging.logAndThrowFatal(logger, errorMessage);
		} catch (RuntimeException e) {}
		verify(logger).error(Logging.LOGGER_FATAL, errorMessage);
	}
	
	@Test
	public void test_logAndThrowFatal_ThrowsRuntimeException() {
		String errorMessage = "MyMessage";
		assertThrowsFatalMessage(errorMessage, () -> Logging.logAndThrowFatal(logger, errorMessage));
	}
	
	@Test
	public void test_logAndThrowFatal_LoggerGetsErrorMessage() {
		String errorMessage = "MyMessage";
		RuntimeException re = new RuntimeException(errorMessage);
		try {
			Logging.logAndThrowFatal(logger, re);
		} catch (RuntimeException e) {}
		verify(logger).error(Logging.LOGGER_FATAL, errorMessage);
	}
	
	@Test
	public void test_logAndThrowFatal_ThrowsGivenException() {
		String errorMessage = "MyMessage";
		RuntimeException re = new RuntimeException(errorMessage);
		assertThrowsFatalMessage(errorMessage, () -> Logging.logAndThrowFatal(logger, re));
	}
}
