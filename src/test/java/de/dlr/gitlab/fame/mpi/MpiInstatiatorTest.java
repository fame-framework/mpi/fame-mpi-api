package de.dlr.gitlab.fame.mpi;

import static de.dlr.gitlab.fame.testUtils.ExceptionTesting.assertThrowsFatalMessage;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import de.dlr.gitlab.fame.mpi.MpiFacade.MpiMode;

public class MpiInstatiatorTest {

	@Test
	public void test_getMpi_ImplementationMissing() {
		assertThrowsFatalMessage(MpiInstantiator.IMPLEMENTATION_MISSING,
				() -> MpiInstantiator.getMpi(MpiMode.NOT_IMPLEMENTED));
	}

	@Test
	public void test_getMpi_InstantiationFailOnPrivateConstructor() {
		assertThrowsFatalMessage(MpiInstantiator.INSTANTIATION_FAIL,
				() -> MpiInstantiator.getMpi(MpiMode.SINGLE_CORE, "dummies.a"));
	}

	@Test
	public void test_getMpi_InstantiationFailOnNoDefaultConstructor() {
		assertThrowsFatalMessage(MpiInstantiator.INSTANTIATION_FAIL,
				() -> MpiInstantiator.getMpi(MpiMode.SINGLE_CORE, "dummies.b"));
	}

	@Test
	public void test_getMpi_MultipleImplementations() {
		assertThrowsFatalMessage(MpiInstantiator.MULTIPLE_IMPLEMENTATIONS,
				() -> MpiInstantiator.getMpi(MpiMode.PARALLEL, "dummies.c"));
	}

	@Test
	public void test_getMpi_ReturnsRequestedMode() {
		MpiFacade facade = MpiInstantiator.getMpi(MpiMode.PARALLEL, "dummies.d");
		assertEquals(MpiMode.PARALLEL, facade.getMode());
		facade = MpiInstantiator.getMpi(MpiMode.SINGLE_CORE, "dummies.d");
		assertEquals(MpiMode.SINGLE_CORE, facade.getMode());
	}

	@Test
	public void test_getMpi_MultiplePackageNames() {
		assertThrowsFatalMessage(MpiInstantiator.MULTIPLE_PACKAGE_NAMES,
				() -> MpiInstantiator.getMpi(MpiMode.NOT_IMPLEMENTED, "a", "b"));
	}
}
