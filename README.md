# FAME-Mpi-api

Provides API for fame parallelisation with MPI

## Available Support
This is a purely scientific project, hence there will be no paid technical support.
Limited support is available, e.g. to enhance FAME, fix bugs, etc. 
To report bugs or pose enhancement requests, please file issues following the provided templates (see also [CONTRIBUTING.md](CONTRIBUTING.md))
For substantial enhancements, we recommend that you contact us via [fame@dlr.de](mailto:fame@dlr.de) for working together on the code in common projects or towards common publications and thus further develop FAME.